import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { NavComponent } from './nav/nav.component';
import { FormComponent } from './form/form.component';
import { GuardExampleComponent } from './guard-example/guard-example.component';
import { TokenService } from './token.service';
import { HttpClientModule } from '@angular/common/http';
import { PlaygroundComponent } from './playground/playground.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TableComponent,
    NavComponent,
    FormComponent,
    GuardExampleComponent,
    PlaygroundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [TokenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
