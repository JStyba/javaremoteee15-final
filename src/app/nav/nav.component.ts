import { Component, OnInit } from '@angular/core';
import { TokenService } from '../token.service';

@Component({
  selector: 'sda-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  // providers: [TokenService] <-- service injected this way is only available in this component (doesn't share state with others using TokenService)
})
export class NavComponent implements OnInit {
  navigation = [{label: 'Table', path: '/table'}, {label: 'Guard Example', path: '/guard-example'}, {label: 'Playground', path: '/playground'}];
  constructor() { }

  ngOnInit(): void {
  }

}
