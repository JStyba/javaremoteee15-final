import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { FunResolver } from './fun.resolver';
import { GuardExampleComponent } from './guard-example/guard-example.component';
import { HomeComponent } from './home/home.component';
import { PlaygroundComponent } from './playground/playground.component';
import { SimpleGuard } from './simple.guard';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'table',
    component: TableComponent,
  },
  {
    path: 'form/add',
    component: FormComponent,
  },
  {
    path: 'form/edit/:id',
    component: FormComponent,
  },
  {
    path: 'guard-example',
    component: GuardExampleComponent,
    canActivate: [SimpleGuard],
  },
  {
    path: 'playground',
    component: PlaygroundComponent,
    resolve: {
      pic: FunResolver,
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
