export interface Country {
    id: number;
    countryName: string;
    population: number;
    flag: string;
    countryCapital: string;
}